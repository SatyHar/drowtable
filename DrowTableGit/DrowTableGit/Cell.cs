﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDrowTable
{
    class Cell
    {
        public virtual byte Column { get; set; }
        public virtual byte Row { get; set; }

        public Cell()
        {
            Column = 1;
            Row = 1;
        }

        public virtual void Draw()
        {
            for (int i = 0; i < Row; i++)
            {
                Console.WriteLine();

                for (int j = 0; j < Column; j++)
                {
                    Console.Write($"{'|'} {'_'}");
                }

                Console.Write('|');
            }
        }
    }
}
