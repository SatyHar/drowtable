﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectDrowTable
{
    class Table : Cell
    {
        public Table()
        {
            Column = 8;
            Row = 8;
        }

        public override void Draw()
        {
            base.Draw();
        }
    }
}
